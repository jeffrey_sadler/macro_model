** End-User License Agreement for Water Resources Systems Analysis through Case Studies

NOTICE
The accompanying software and content package (�Product�) is licensed to you by ASCE
under the terms set forth in this End-User License Agreement (�License Agreement�) and
is conditioned upon your acceptance of, and compliance with, these terms. Please read
this License Agreement carefully before downloading and unzipping the software, as
your downloading the software will indicate your agreement to all terms contained
herein.

LICENSE AGREEMENT
This Product is owned by the American Society of Civil Engineers (ASCE) and its
licensors. Your right to use the Product is governed by the terms and conditions of
this agreement. No other use of the Product is permitted without express written
authorization from ASCE.

LICENSE
Throughout this License Agreement, �you� shall mean either the individual or the
entity whose agent downloads the software. You are granted a limited, nonexclusive,
and nontransferable license to use the Product subject to the following terms: (i) The
Product may only be used on a single computer (i.e., a single CPU). (ii) You may make
one copy of the Product for back-up purposes only and you must maintain an accurate
record as to the location of the back-up at all times.

COPYRIGHT, RESTRICTIONS ON USE, AND TRANSFER
All rights (including copyright) in and to the Product are owned by ASCE and its
licensors. You may not decompile, modify, reproduce, create derivative works, transmit,
distribute, sublicense, store in a database, rent or transfer the Product, or any
portion thereof, in any form or by any means (including electronically or otherwise)
except as expressly provided for in this License Agreement. You must reproduce the
copyright notices, trademark notices, and logos of ASCE and its licensors that appear
on the Product on the back-up copy of the Product which you are permitted to make
hereunder. All other rights in the Product not expressly granted herein are reserved by
ASCE and its licensors.

TERM
This License Agreement is effective until terminated. It will terminate if you fail to
comply with any term or condition of this License Agreement. Upon termination, you are
obliged to return to ASCE the Product together with all copies thereof and to purge all
copies of the Product included in any and all servers and computer facilities.

DISCLAIMER OF WARRANTY
This Product is provided to you on an �as is� basis. ASCE and its licensors make no
representation as to results to be obtained by any person or entity from use of the
Product and/or any information or data included therein. ASCE and its licensors
expressly disclaim all warranties, express or implied, in and to the Product including
without limitation any warranties of merchantability or fitness for a particular
purpose or use. Neither ASCE nor its licensors warrant that the functions contained in
the product will meet your requirements or that the operation of the product will be
uninterrupted or error free. You assume the entire risk with respect to the quality and
performance of the Product or your reliance on any information contained or provided
therein. This Product is provided to you subject to the understanding that ASCE and its
licensors are not engaged in providing engineering or other professional services. If
such services are required, the assistance of an appropriate professional should be
sought.

LIMITATION OF LIABILITY
Neither ASCE nor its licensors shall be liable for any direct, indirect, special, or
consequential damages, (including, but not limited to, breach of express or implied
contract; loss of use, data or profits; business interruption; or damage to any
equipment, software and/or data files), however caused and on any legal theory of
liability, whether for contract, tort, strict liability, or a combination thereof
(including negligence or otherwise) arising in any way out of the direct or indirect
use of the Product, even if advised of the possibility of such risk and potential
damage. This limitation of liability shall apply to any claim or cause whatsoever
whether such claim or cause arises in contract, tort, or otherwise. Some states do not
allow the exclusion or limitation of indirect, special or consequential damages, so the
above limitation may not apply to you.

U.S. GOVERNMENT END USER RESTRICTED RIGHTS
Any software included in the Product is provided with restricted rights subject to
subparagraphs (c) (1) and (2) of the Commercial Computer Software-Restricted Rights
clause at 48 C.F.R. 52.227-19. The terms of this Agreement applicable to the use of the
data in the Product are those under which the data are generally made available to the
general public by ASCE. Except as provided herein, no reproduction, use, or disclosure
rights are granted with respect to the Product or any data therein and no right to
modify or create derivative works from the Product or any such data is hereby granted.

GENERAL
The agreement will be governed by the laws of the Commonwealth of Virginia.

Copyright � 2013 by the American Society of Civil Engineers.
All Rights Reserved.