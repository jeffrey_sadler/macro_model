import pandas as pd
import re
import numpy as np


# define inputs
# 'o' represents 'original, 'n' represent 'new'
class ModelInput:
    o_tunnel_volume = 54144385.  # ft3
    inc_tunnel_vol_cost = 15./10**6  # million $/ft3
    o_vrssi_min = 5414438.  # ft3
    n_pump_rate = 45.  # cfs
    inc_q_cost = 5.  # million $/cfs
    o_max_q_jones = 140.  # cfs
    o_max_q_south_shore = 45.  # cfs
    o_hi_siphon_flow = 256.  # cfs
    o_lo_siphon_flow = 217.  # cfs
    blending_limit = 94.
    o_jlimit = 511.
    jilimits = '511 464 441 412 382 353 323 294 286 277 268 258 248'
    jilimits_array_strs = np.array(jilimits.split())
    jilimits_array_ints = jilimits_array_strs.astype(int)
    jilimits_array_ratios = jilimits_array_ints / o_jlimit

    def __init__(self, inc_tunnel_vol_mult=1, change_vrssi_min=1, inc_pumps_jones=0,
                 inc_pumps_south_shore=0, upgrade_hi_siphon_flow=False,
                 upgrade_lo_siphon_flow=False, inc_j_trt_cap=1, start_year=1940, end_year=2004,
                 outfilename='mit111'):
        # tunnel volume
        self.n_tunnel_vol = self.o_tunnel_volume * inc_tunnel_vol_mult
        self.inc_tunnel_vol = self.n_tunnel_vol - self.o_tunnel_volume  # ft3
        self.tunnel_vol_cost = self.inc_tunnel_vol * self.inc_tunnel_vol_cost  # $

        # vrssimin
        self.n_vrssi_min = self.o_vrssi_min*change_vrssi_min  # ft3

        # max pumping rates to wwtps
        self.n_max_q_jones = self.o_max_q_jones + inc_pumps_jones*self.n_pump_rate  # cfs
        self.n_max_q_s_shore = self.o_max_q_south_shore + inc_pumps_south_shore*self.n_pump_rate
        self.cost_q_jones = inc_pumps_jones*self.inc_q_cost  # $
        self.cost_q_s_shore = inc_pumps_south_shore*self.inc_q_cost  # million $

        # siphon capacities
        self.n_hi_siphon_flow = 400. if upgrade_hi_siphon_flow else self.o_hi_siphon_flow  # cfs
        self.n_lo_siphon_flow = 400. if upgrade_lo_siphon_flow else self.o_lo_siphon_flow  # cfs
        self.hi_siphon_cost = 12. if upgrade_hi_siphon_flow else 0.  # million $
        self.lo_siphon_cost = 12. if upgrade_lo_siphon_flow else 0.  # million $

        # increase in jones treatment limits
        self.n_jlimit = self.o_jlimit * inc_j_trt_cap
        self.inc_trt_cost = self.n_jlimit - self.o_jlimit
        self.n_jlimits = self.n_jlimit * self.jilimits_array_ratios
        self.n_jchlor_limit = self.n_jlimit + self.blending_limit

        # other inputs
        self.start_year = start_year
        self.end_year = end_year
        self.outputfilename = outfilename

        self.inputs = {
            'start_year': start_year,
            'end_year': end_year,
            'outfilename': outfilename,
            'tunnel_volume': self.n_tunnel_vol,
            'vrssi_min': self.n_vrssi_min,
            'max_q_jones': self.n_max_q_jones,
            'max_q_south_shore': self.n_max_q_s_shore,
            'hi_siphon_flow': self.n_hi_siphon_flow,
            'lo_siphon_flow': self.n_lo_siphon_flow,
            'jtrtmnt_limit': self.n_jlimit,
            'jchlor_limit': self.n_jchlor_limit,
            'jlimits': " ".join(str(i) for i in self.n_jlimits)
        }

        self.total_cost = self.tunnel_vol_cost + self.cost_q_jones + self.cost_q_s_shore + \
            self.hi_siphon_cost + self.lo_siphon_cost  # million $


class ModelOutput:
    def __init__(self, out_text):
        self.out_text = out_text
        self.summary_table = self.get_summary_of_spill_events_table(out_text)
        self.total_cs_spills = self.summary_table['CS_SPILLS'].sum()
        self.total_cs_vol = self.summary_table['CS_VOL'].sum()
        self.total_ss_spills = self.summary_table['SS_SPILLS'].sum()
        self.total_ss_vol = self.summary_table['SS_VOL'].sum()

    def get_summary_of_spill_events_table(self, output_txt):
        dash_begin = output_txt.find('---- ')
        txt = output_txt[dash_begin:]
        table_begin = txt.find('\n') + 1
        txt = txt[table_begin:]
        dash_end = txt.find('---- ')
        table_end = dash_end - 2
        table = txt[:table_end]
        table = table.split('\n')
        table = [re.split(' +', a.strip()) for a in table]
        column_names = ['YEAR',
                        'FULL_TUNNEL',
                        'CS_SPILLS',
                        'CS_VOL',
                        'SS_SPILLS',
                        'SS_VOL']
        df = pd.DataFrame(table, columns=column_names)
        for col in list(df):
            df.loc[:, col] = pd.to_numeric(df.loc[:, col])
        return df

