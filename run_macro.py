from subprocess import Popen
from input_output_cls_def import ModelInput, ModelOutput
from deap import base, creator, tools, algorithms
import random
import numpy as np
import pandas as pd
import datetime
import matplotlib.pyplot as plt


def make_new_input_file(inputs, input_file_name):
    # read in template
    template_f = open('mitchell_template.txt', 'r')
    txt = template_f.read()
    template_f.close()
    changed_input = txt.format(**inputs)

    # write changed inputs
    run_f = open('{}.cmm'.format(input_file_name), 'w')
    run_f.write(changed_input)
    run_f.close()

    # change the text file that the .bat file reads
    info_f = open('input_info.txt', 'w')
    info_f.write('{}.cmm'.format(input_file_name))


def create_random_decision_variables():
    inc_tunnel_vol = random.randint(0, 50000)
    vrssi_multiplier = 2*random.random()
    inc_pump_jones = random.randint(0, 20)
    inc_pump_s_shore = random.randint(0, 20)
    upgrade_hi_siphon = random.randint(0, 1)
    upgrade_lo_siphon = random.randint(0, 1)
    return [inc_tunnel_vol, vrssi_multiplier, inc_pump_jones, inc_pump_s_shore, upgrade_hi_siphon,
            upgrade_lo_siphon]


def get_model_input(individual):
    parameters_names = [
        'inc_tunnel_vol_mult',
        'change_vrssi_min',
        'inc_pumps_jones',
        'inc_pumps_south_shore',
        'upgrade_hi_siphon_flow',
        'upgrade_lo_siphon_flow',
        'inc_j_trt_cap'
    ]
    return ModelInput(**dict(zip(parameters_names, individual)))


def run_model(inputs):
    input_file_name = 'mitchell_mod'
    make_new_input_file(inputs, input_file_name)
    # run model
    print 'Running model'
    p = Popen('RUNMACRO.BAT')
    p.wait()

    # read outputs
    out_rpt_f = open('{}.RPT'.format(inputs['outfilename']))
    out_rpt_txt = out_rpt_f.read()
    return ModelOutput(out_rpt_txt)


def bit_to_decimal(l):
    d_str = "".join(str(i) for i in l)
    return int(d_str, 2)


def convert_to_mulipliers(l):
    for i in range(len(l)):
        if i == 0 or i == 6:
            l[i] /= 255.
            l[i] += 1
            if l[i] > 2 or l[i] < 1:
                raise ValueError('The value is not what we expected')
        elif i == 1:
            l[i] /= 255.
            l[i] += .5
            if l[i] > 1.5 or l[i] < .5:
                raise ValueError('The value is not what we expected')

    return l


def get_individual_bits(individual):
    b1 = individual[:8]  # first byte is for decision variable 1
    b2 = individual[8:16]  # second byte is for decision variable 2
    b3 = individual[16:20]  # these bits are for decision variable 3
    b4 = individual[20:24]  # these bits are for decision variable 3
    b5 = [individual[24]]   # this bit is for decision variable 4
    b6 = [individual[25]]  # this bit is for decision variable 5
    b7 = individual[26:]  # this bit is for decision variable 6
    return b1, b2, b3, b4, b5, b6, b7


def get_decimals(individual):
    bits = get_individual_bits(individual)
    decimals = [bit_to_decimal(i) for i in bits]
    return convert_to_mulipliers(decimals)


def bit_string_to_input(bitstring):
    params = get_decimals(bitstring)
    return get_model_input(params)


def evaluate(individual):
    model_input = bit_string_to_input(individual)
    model_output = run_model(model_input.inputs)
    print "total csos: ", model_output.total_cs_spills
    print "total ssos: ", model_output.total_ss_spills
    print "capital cost: ", model_input.total_cost

    # here is where we need to decide on the criteria
    total_cost = model_input.total_cost + (50*model_output.total_cs_spills)**2 + \
                 (50*model_output.total_ss_spills)**4

    print "fitness: ", total_cost

    return total_cost,

creator.create('FitnessMin', base.Fitness, weights=(-1.0,))
creator.create('Individual', list, fitness=creator.FitnessMin)

toolbox = base.Toolbox()
toolbox.register("attr_bool", random.randint, 0, 1)
toolbox.register("individual", tools.initRepeat, creator.Individual,
                 toolbox.attr_bool, 33)
toolbox.register('population', tools.initRepeat, list, toolbox.individual)
toolbox.register('evaluate', evaluate)
toolbox.register("mate", tools.cxTwoPoint)
toolbox.register("mutate", tools.mutFlipBit, indpb=0.10)
toolbox.register("select", tools.selTournament, tournsize=6)


def plot_results(d):
    df = pd.read_csv('docs/results_{}.csv'.format(d))
    df_plt = df[['avg', 'max', 'min']]
    ax = df_plt.plot()
    ax.set_ylabel('millions of $')
    ax.set_xlabel('generation')
    ax.set_title('')
    plt.savefig('docs/results{}.png'.format(d), dpi=300)


def main():
    ngen = 10
    npop = 20
    pop = toolbox.population(n=npop)
    hof = tools.HallOfFame(1)
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", np.mean)
    stats.register("min", np.min)
    stats.register("max", np.max)
    beg_time = datetime.datetime.now().strftime("%Y.%m.%d.%H.%M")
    f = open('log.txt', 'a')
    f.write('run started: {}'.format(beg_time))
    pop, logbook = algorithms.eaSimple(pop, toolbox, cxpb=0.5, mutpb=0.2, ngen=ngen, stats=stats,
                                       halloffame=hof, verbose=True)
    end_time = datetime.datetime.now().strftime("%Y.%m.%d.%H.%M")
    f.write('run ended: {}\n'.format(end_time))
    f.close()

    df = pd.DataFrame(logbook)
    df.to_csv('docs/results_{}csv'.format(end_time, index=False))

    f = open('docs/hof.txt', 'a')
    for h in hof:
        f.write('hof for {}:{}    fitness:{}\n'.format(end_time, h, h.fitness))
    f.close()
    plot_results(end_time)

    return pop, logbook, hof

if __name__ == "__main__":
    # pop, log, hof = main()
    # evaluate([1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0])
    plot_results('2016.12.08.10.29')

