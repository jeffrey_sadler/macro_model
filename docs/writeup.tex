\documentclass[]{article}
\usepackage[margin=1in]{geometry}
\usepackage{amsmath}
\usepackage{tabularx}
\usepackage{hyperref}
\usepackage{graphicx}
\graphicspath{{.}}
%opening
\title{Environmental Systems Analysis Semester Project: Applying Genetic Algorithm to an Opaque CSO, SSO model, MACRO}
\author{Jeff Sadler}
\begin{document}
	\maketitle
	\section{Background}
	The city of Milwaukee Wisconsin's stormwater and wastewater share the same pipe infrastructure. RECENTLY the city invested millions of dollars to construct an underground storage unit where stormwater from intense storms can be stored to reduce combined sewer overflows. To simulate the system of receiving pipes, storage and wastewater treatment, the MACRO model was built. MACRO models the performance of the system when given the built-in inflows originating from historic records, and a number of decision variables as described below. The time period over which the model was run for this project is 1940-2004. The decision variables were based on five options for adjusting either the physical infrastructure capacity or operation policies (see Table \ref{table1}). The outcomes from MACRO focused on in this project were the number of combined sewer overflows (CSOs) and sewer system overflows (SSOs). The purpose of this project was to find a near-optimum solution of minimizing the cost of capital improvement for reducing CSOs and SSOs.
		\begin{table}[h!]
			\centering
			\caption{Capital investment and operational options for reducing SSOs and CSOs}
			\begin{tabularx}{0.8\linewidth}{|l | X | l | X|}
				\hline
				\textbf{Measure} & \textbf{Cost} \\
				\hline
				Increase Tunnel (ISS) volume &$ \$15/\textrm{ft}^3 $\\
				\hline
				Adjust VRSSImin & None\\
				\hline
				Increase pumping capacities from ISS to WWTPs & 45 cfs pump costs \$5 million\\
				\hline
				Increase siphon capacity at JIWWTP & Each siphon costs \$12 million to upgrade to 400 cfs\\
				\hline
				Increase treatment capcity at JIWWTP& \$1 million/cfs\\
				\hline
			\end{tabularx}
			\label{table1}
		\end{table}
	\section{Formulation}
	Because the mathematical form of the MACRO model is unknown a truly optimal solution to the mathematical model is not possible, however, using a genetic algorithm should be able to produce a practically optimal solution. Because any solution found can be implemented only with limited precision based on construction practices etc., a practically optimal solution should be sufficient in this case. Additionally, because the mathematical formula governing the MACRO model is unknown, there are few if any other choices.
	\subsection{Objective Statement}
	Minimize total number of CSOs and SSOs using least amount of money possible.
	\subsection{Definition of Decision Variables}
	The decision variables, shown in Table \ref{table:decision_vars} were based on the capital improvement and operation operations listed in Table \ref{table1}. Some of these were straightforward to implement in the model such as $s1$ and $s2$ which were simple binary variables and $V$ which was a percentage of the total tunnel volume. For the rest of the variables, I had to put some arbitrary bounds that seemed reasonable to me. These are seen in the Range column of Table \ref{table:decision_vars}.
	\begin{table}
		\caption{Description of decision variables}
		\begin{tabularx}{\linewidth}{|l|X|l|X|l|X|l|X| l|X|}
		\hline 
	\textbf{Variable Symbol }& \textbf{Description }& \textbf{Type} &\textbf{ Range} & \textbf{units} \\ 
		\hline 
		$T$ & amount to increase tunnel volume & continuous &0 and the original tunnel volume, 54,144,385  & ft$^3$ \\ 
		\hline 
		$V$ &  portion of storage tunnel $T$ to reserve for inflows & continuous  & 0-1 & none \\ 
		\hline 
		 $n_1$& number of pumps to increase pumping from ISS to Jones Island WWTP  & integer  & 0-15 & pumps\\ 
		\hline 
		$n_2$& number of pumps to increase pumping from ISS to South Shore WWTP &  integer  & 0-15 & pumps\\ 
		\hline 
		 $s_h$ & whether or not to upgrade hi-flow siphon  & binary (0= no upgrade, 1=upgrade)& 0, 1 & none \\ 
		\hline 
		$s_l$ & whether or not to upgrade lo-flow siphon & binary (0= no upgrade, 1=upgrade) & 0, 1 & none \\ 
		\hline 
		$q_j$& amount to increase treatment capacity at Jones Island WWTP & continuous & 0 and original capacity, 511 & cfs\\ 
		\hline 
	\end{tabularx} 
	\label{table:decision_vars}
\end{table}

	\subsection{Objective Function and Constraints}
	In other optimization problems given this semester, for which the mathematical model was known, strict constraints were given that bounded the the objective function. For example, a minimum dissolved oxygen level. In this case, in which the mathematical formula was not known and a genetic algorithm was required, this approach was not possible and the 'constraints' are implemented as part of the objective function (Eq. 1) as penalties. This is seen in the final two terms of the objective function $50*CSO^2$ and  $50*SSO^4$. The genetic algorithm is seeking to minimize the cost and since the number of CSOs and SSOs contributes substantially to the cost, the algorithm will be incentivized to reduce those. 
	\begin{multline}
		 \textrm{Z [million \$]} = T[\textrm{ft}^3]*15[\$/ft^3]*1/10^6+n_1[pumps]*5[10^6\$/pump]+ \\n_2*5[10^6\$/pump]+s_h*12[10^6\$]+s_l*12[10^6\$]+ 
		 \\q_j[cfs]*1[10^6\$/cfs]	+ 50*CSO^2 + 50*SSO^4
	\end{multline}
	\subsection{Solution}
	I used the genetic algorithm from the python package, DEAP (\hyperref[https://github.com/deap/deap]{https://github.com/deap/deap}). I wrote python scripts to automatically update the input file used by MACRO given the individuals produced using the DEAP library in the running of the genetic algorithm. Each individual policy was used to run the MACRO model and the output CSOs and SSOs (plus the capital cost corresponding to the policy; see Eq. 1)  was considered the individual's fitness. The code I wrote to accomplish this is available at \hyperref[https://bitbucket.org/jeffrey_sadler/macro_model/src]{https://bitbucket.org/jeffrey\_sadler/macro\_model}. For the genetic algorithm parameters I used independent mutation probability rate of 0.1, a tournament selection approach with a tournament size of 6, a crossover probability of 0.5, and a mutation probability of 0.2. 
	
	 To simplify the running of the genetic algorithm, I converted the decision variables to one or more bits. These are described in Table \ref{table:bit_descr}. In total, one individual policy was represented by a string of 34 bits. In converting the decision variables to bits, the continuous decision variables ($T$, $V$, and $q_j$) became non-continuous. The rest of the variables, being either integer or binary variables needed no conversion. The conversion of the continuous variables is as follows, 
	\begin{align*}
		 T &= (1+[bits_{1-8}]/255)*T_{orig}\\
		 V &= [bits_{9-16}/255]\\
		 q_j &= (1+[bits_{27-34}]/255)*q_{j-orig}
	\end{align*}

\begin{table}[h!]
	\centering
\caption{Description of the mapping of decision variables to bits}
\begin{tabular}{|l|l|}
	\hline 
	\textbf{Decision Variable} &\textbf{ Bits} \\ 
	\hline 
	$T$& 1-8 \\ 
	\hline 	
	$V$ & 9-16 \\ 
	\hline 
	$n_1$ & 17-20 \\ 
	\hline 
	$n_2$ & 21-24 \\ 
	\hline 
	$s_1$ & 25 \\ 
	\hline 
	$s_2$ & 26 \\ 
	\hline 
	$q_j$ & 27-34 \\ 
	\hline 
	\end{tabular} 
		\label{table:bit_descr}
	\end{table}
\subsubsection{Results}
The best policy resulting from running ten generations with a starting population of 20 individuals is listed in Table \ref{table:best_policy}.  The total real cost of this policy was \$899 million and resulted in a total of 40 CSOs and 8 SSOs. The objective function cost was \$25x10$^9$ million. Figure \ref{fig:summary} shows the progression toward the best policy over the ten generations. 
\begin{table}[h!]
	\centering
	\caption{Best policy found from genetic algorithm with starting population of 20 individuals and 10 generations}
	\begin{tabular}{|l|l|l|l|l}
		\hline 
		\textbf{Decision Variable} &\textbf{ Value} & \textbf{Units}& \textbf{Cost [10$^6$\$]}\\ 
		\hline 
		$T$& 91,726,958 & ft$^3$& 563\\ 
		\hline 	
		$V$ & 0.88 & none& N/A\\ 
		\hline 
		$n_1$ & 12 & pumps& 60\\ 
		\hline 
		$n_2$ & 7 & pumps& 35\\ 
		\hline 
		$s_1$ & 0 & none& 0\\ 
		\hline 
		$s_2$ & 0 & none& 0\\ 
		\hline 
		$q_j$ & 751 & cfs& 240\\ 
		\hline 
	\end{tabular} 
	\label{table:best_policy}
\end{table}
\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.5]{results2016_12_08_10_29.png}
	\caption{Progression of objective function cost over algorithm run}
	\label{fig:summary}
\end{figure}

\section{Conclusion}
I was able to successfully use a genetic algorithm to begin to attain a practically optimum solution for the opaque MACRO model. The trend in Figure \ref{fig:summary} indicates that the software is working as intended (i.e. the overall  objective function values are decreasing over the generations). If I were to continue to work on this, I would like to run the genetic algorithm for more generations and with a larger population. The algorithm did not reach an optimum policy with the given number of individuals and generations. I confirmed this by running the model with a policy identical to the policy chosen by the genetic algorithm (Table \ref{table:best_policy}) but with maximizing $T$ so that it was $2T_{orig}$. The results were an objective function value of less than half (\$8x10$^9$ million) of the policy settled on by the genetic algorithm above. From this, I conclude that running the algorithm for more generations would result in a policy that would at least match the performance of the manually adjusted policy and would probably exceed it. Unfortunately, since running with the 10 generations and 20 starting individuals took around 24 hours, time did not allow for testing this hypothesis.
 
\end{document}
